from django.views.generic import TemplateView, ListView
from .models import About
# from django.core.mail import send_mail

class HomePageView(TemplateView):
	template_name = 'home.html'
	# send_mail(
	#     'Aguará Tech',
	#     'Limon limon',
	#     'agwallpy@gmail.com',
	#     ['marceloagoston91@gmail.com',],
	#     # auth_user = 'Login'
	#     # auth_password = 'Password'
	#     fail_silently = False,
	# )
class AboutPageView(ListView):
	template_name = 'about.html'
	model = About
	context_object_name = 'contexto'
