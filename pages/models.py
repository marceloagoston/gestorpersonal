from django.db import models
from tinymce.models import HTMLField

class About(models.Model):
	titulo = models.CharField(max_length=20)
	texto = HTMLField()
	# avatar = models.ImageField(blank=True,null=True,upload_to="imagenes/%Y/%m/")
	def __str__(self):
		return str(self.id) + self.titulo

