from django.contrib import admin
from .models import About

# class BigWalletAdmin(admin.ModelAdmin):
# 	model = BigWallet
# 	list_display = ['id','fecha','monto','descripcion',]
# 	# list_filter = ['resp_seguridad','tipoactivo',]
# 	# search_fields = ['resp_seguridad__username','tipoactivo',]
admin.site.register(About)
# admin.site.register(BigWallet, BigWalletAdmin)