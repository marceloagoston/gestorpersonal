from djmoney.models.fields import MoneyField
from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from users.models import CustomUser
from django.urls import reverse

# class SalaCompartida(models.Model):
#     nombre = models.CharField(max_length=20)
#     creador = models.ForeignKey(
#         CustomUser,
#         on_delete=models.CASCADE,
#         related_name="CreadorSala"
#         )
#     participantes = models.ManyToManyField(CustomUser, related_name="Participantes")

#     def __str__(self):
#         return 'Nombre {}, ID: {}, Creador {}'.format(self.nombre, self.id, self.creador) 

class BigWallet(models.Model):
    fecha = models.DateTimeField(auto_now_add=False)
    monto = MoneyField(max_digits=14, decimal_places=2, default_currency='ARS')
    descripcion = models.TextField()
    select_tipo =[
    ('Alimentos', 'Alimentos'),
    ('Alimentos Gula', 'Alimentos Gula'),
    ('Ingresos', 'Ingresos'),
    ('Ingresos', 'Salud'),
    ('Delivery', 'Delivery'),
    ('Deportes', 'Deportes'),
    ('Pepeleria', 'Pepeleria'),
    ('Ocio/Hobby', 'Ocio/Hobby'),
    ('Electronica', 'Electronica'),
    ('Salida', 'Salida'),
    ('Farmacia', 'Farmacia'),
    ('Combustibles', 'Combustibles'),
    ('Bicicleta', 'Bicicleta'),
    ('Video Juegos', 'Video Juegos'),
    ('Inversiones', 'Inversiones'),
    ('Viajes', 'Viajes'),
    ('Servicios', 'Servicios'),
    ('Mascotas', 'Mascotas'),
	('Dummy', 'Dummy'),    
    ('Otros', 'Otros'),
	]
    tipo = models.CharField(max_length=20,choices=select_tipo,default='Otros')
	# Proxima release agregar tipo de gasto que pueda ser null
    persona = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        )

    # compartido = models.ForeignKey(
    #     SalaCompartida,
    #     on_delete=models.CASCADE
    #     )

    def __str__(self):
       return self.descripcion

    def get_absolute_url(self):
        return reverse('walletlist')

    class Meta:
        ordering = ['-fecha']

