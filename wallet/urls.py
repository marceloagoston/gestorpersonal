from django.urls import path
from .views import WalletListView, WalletCreateView, WalletUpdateView, WalletDeleteView

urlpatterns = [
	path('', WalletListView.as_view(), name='walletlist'),
	path('crearmovmiento/', WalletCreateView.as_view(), name='altamov'),
	path('actualizarmov/<int:pk>', WalletUpdateView.as_view(), name='actualizarmov'),
	path('borrarmov/<int:pk>', WalletDeleteView.as_view(), name='borrarmov'),
]