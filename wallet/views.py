from django.shortcuts import render
from .models import BigWallet
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from django.db.models import Sum
import datetime
from django.utils import timezone

class WalletListView(LoginRequiredMixin ,ListView):
	model = BigWallet
	template_name = 'wallet/mainwallet.html'
	context_object_name = 'movimientos'
	login_url = 'login'
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		x= BigWallet.objects.filter(persona=self.request.user.id).aggregate(Sum('monto'))['monto__sum']
		context['total'] =  float(x if x is not None else 0)
		return context
	
	def get_queryset(self):
		return BigWallet.objects.filter(persona=self.request.user.id)		

class WalletCreateView(LoginRequiredMixin ,CreateView):
	model = BigWallet
	template_name = 'wallet/abm/crearmovimiento.html'
	fields = ('tipo','monto', 'descripcion')
	login_url = 'login'

	def form_valid(self, form):
		form.instance.persona = self.request.user
		form.instance.fecha = timezone.now()
		return super().form_valid(form)

class WalletUpdateView(LoginRequiredMixin ,UpdateView):
	model = BigWallet
	template_name = 'wallet/abm/actualizarmovimiento.html'
	fields = ('monto','tipo', 'descripcion','fecha')
	context_object_name = 'movimiento'
	login_url = 'login'

	def dispatch(self, request, *args, **kwargs):
		obj = self.get_object()
		if obj.persona != self.request.user:
			raise PermissionDenied
		return super().dispatch(request, *args, **kwargs)

class WalletDeleteView(LoginRequiredMixin ,DeleteView):
	model = BigWallet
	template_name = 'wallet/abm/borrarmovimiento.html'
	context_object_name = 'movimiento'
	success_url = reverse_lazy('walletlist')
	login_url = 'login'

	def dispatch(self, request, *args, **kwargs):
		obj = self.get_object()
		if obj.persona != self.request.user:
			raise PermissionDenied
		return super().dispatch(request, *args, **kwargs)
